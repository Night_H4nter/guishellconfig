(in-package :stumpwm)



(setf *mode-line-timeout* 2)
; (setf *time-modeline-string* "%F %H:%M")
(setf *window-format* "%n: %30t")
(setf *time-modeline-string* "%a, %b %d %I:%M%p")
(setf *screen-mode-line-format*
      (list
       ;; Groups
       ; " ^7[^B^4%n^7^b] "
       "%g"
       ;; windows
       ; "%w"
       ;; Pad to right
       "^>"
       '(:eval (when (> *reps* 0)
                 (format nil "^1^B(Reps ~A)^n " *reps*)))
       ;; Date
       "^7"
       "%d"
       ))

(defun enable-mode-line-everywhere ()
  (loop for screen in *screen-list* do
    (loop for head in (screen-heads screen) do
	    (enable-mode-line screen head t))))
(enable-mode-line-everywhere)
