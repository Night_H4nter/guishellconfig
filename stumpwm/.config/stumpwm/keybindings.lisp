(in-package :stumpwm)
(which-key-mode)


(setf stumpwm:*root-map* (stumpwm:make-sparse-keymap))
(setf stumpwm:*groups-map* (stumpwm:make-sparse-keymap))
; (setf stumpwm:*top-map* (stumpwm:make-sparse-keymap))


; (set-prefix-key (kbd "C-SPC"))


; (defvar *my-apps-keymap*
;   (let ((m (make-sparse-keymap)))
; m))

; (defvar *top-map* (let ((m (stumpwm:make-sparse-keymap)))
; m))


(define-key *top-map* (kbd "s-j") "move-focus left")
(define-key *top-map* (kbd "s-k") "move-focus down")
(define-key *top-map* (kbd "s-l") "move-focus up")
(define-key *top-map* (kbd "s-semicolon") "move-focus right")

(define-key *top-map* (kbd "s-J") "exchange-direction left")
(define-key *top-map* (kbd "s-K") "exchange-direction down")
(define-key *top-map* (kbd "s-L") "exchange-direction up")
(define-key *top-map* (kbd "s-colon") "exchange-direction right")

(define-key *top-map* (kbd "s-Return") "exec alacritty")


(defvar *my-end-session-keymap*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "q") "end-session")
    (define-key m (kbd "l") "logout")
    ; (define-key m (kbd "s") "suspend-computer")
    ; (define-key m (kbd "S") "shutdown-computer")
    (define-key m (kbd "r") "loadrc")
    (define-key m (kbd "R") "restart-hard")
    ; (define-key m (kbd "C-r") "restart-computer")
m))


(define-key *top-map* (kbd "s-Q") '*my-end-session-keymap*)


; (define-key *top-map* (kbd "s-S-C-j") "move-window left")
; (define-key *top-map* (kbd "s-S-C-k") "move-window down")
; (define-key *top-map* (kbd "s-S-C-l") "move-window up")
; (define-key *top-map* (kbd "s-S-C-semicolon") "move-window right")

