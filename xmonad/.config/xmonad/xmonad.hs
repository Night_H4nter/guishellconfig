--imports {{{
--common lib
import XMonad
--helper
import qualified Data.Map as M
--hotkeys (and layout?)
import qualified XMonad.StackSet as W
import XMonad.Layout.SimpleFloat
import XMonad.Layout.Spacing
--exiting
import System.Exit
--multihead support
import XMonad.Layout.IndependentScreens
--dock support
import XMonad.Hooks.ManageDocks
--better navigation
import XMonad.Actions.Navigation2D
--submap for key sequences
import XMonad.Actions.Submap
--for startup hook
import XMonad.Util.SpawnOnce
--to fool java crap
import XMonad.Hooks.SetWMName
--for ewmh
import XMonad.Hooks.EwmhDesktops
--fullscreen handler
import XMonad.Hooks.ManageHelpers
-- import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
--taffybar
import XMonad.Hooks.TaffybarPagerHints
--decorations
-- import XMonad.Layout.NoFrillsDecoration
-- import XMonad.Layout.DecorationAddons
-- import XMonad.Layout.ButtonDecoration
--}}}




--startup hook{{{
myStartupHook :: X ()
myStartupHook = do
    --dumb autostart
    spawnOnce "~/.dotfiles/deconfig/scripts/autostart"
    -- spawnOnce "taffybar"
    spawnOnce "polybar"
    setWMName "LG3D"
--}}}




-- apps {{{
myTerminal = "alacritty"
myScreenshotTool = "flameshot gui" --TODO:get rid of it
myWebBrowser = "flatpak run com.google.Chrome"
myDiscordClient = "flatpak run com.discordapp.Discord"
myTelegramClient = "flatpak run org.telegram.desktop"
myEmailClient = "evolution"
myTuiFm = myTerminal ++ " -e vifm"
-- myGuiFm = "dolphin"
myGuiFm = "nautilus"
---}}}




-- appearance {{{
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myBorderWidth = 3
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"
--}}}




-- layout settings {{{
-- wsArray = ["tab", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p"]
-- myExtraWorkspaces = [ (xK_0, "10") ]
-- wsArray = map show [1..9] ++ (map snd myExtraWorkspaces)
wsArray = map show [ 1..12 ]
wsKeys = [ xK_Tab, xK_q, xK_w, xK_e, xK_r, xK_t, xK_y, xK_u, xK_i, xK_o, xK_p, xK_backslash ]
-- wsArray = zip wsKeys wsNameList
-- wsArray = [ xK_Tab, xK_q, xK_w, xK_e, xK_r, xK_t, xK_y, xK_u, xK_i, xK_o, xK_p, xK_backslash ]


myLayoutHook = spacing 10 $ avoidStruts $ tiled ||| Mirror tiled ||| simpleFloat
    where
        tiled = Tall nmaster delta ratio
        nmaster = 1
        ratio = 1/2
        delta = 3/100
-- }}}




-- workspace keys {{{

--focus/move window to workspace
myWorkspaceKeys conf = M.fromList $
    [((modifier .|. mod4Mask, key), windows ( onCurrentScreen action targetWorkspace ) )
        | (targetWorkspace, key) <- zip (workspaces' conf) wsKeys
        , (action, modifier) <- [(W.view, 0), (W.shift, shiftMask)]]
    
    ++

    [((modifier .|. mod4Mask, key), screenWorkspace sc >>= flip whenJust (windows . action))
        | (key, sc) <- zip [xK_m, xK_comma, xK_period] [0..]
        , (action, modifier) <- [(W.view, 0), (W.shift, shiftMask)]]
--}}}




-- launcher keys {{{
myLauncherKeys conf = M.fromList

    -- launch a terminal
    [ ((mod4Mask, xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu
    , ((mod4Mask,               xK_d     ), spawn "`dmenu_path | dmenu` && eval \"exec $exe\"")

    -- launch other apps
    , ((mod4Mask, xK_s),  submap . M.fromList $
    [ ((0,           xK_w), spawn $ myWebBrowser)
    , ((0,           xK_d), spawn $ myDiscordClient)
    , ((0,           xK_t), spawn $ myTelegramClient)
    , ((0,           xK_e), spawn $ myEmailClient)
    , ((0,           xK_f), spawn $ myTuiFm)
    , ((shiftMask,   xK_f), spawn $ myGuiFm) ]) ]
-- }}}

toggleFloat w = windows (\s -> if M.member w (W.floating s)
        then W.sink w s
        else (W.float w (W.RationalRect (1/3) (1/4) (1/2) (4/5)) s))


-- window keys {{{
myWindowKeys conf = M.fromList
    -- close focused window
    [ ((mod4Mask .|. shiftMask, xK_c     ), kill)

    -- Switch between layers
    -- , ((mod4Mask,                 xK_space), switchLayer)

    -- Directional navigation of windows
    , ((mod4Mask,                 xK_semicolon), windowGo R False)
    , ((mod4Mask,                 xK_j ), windowGo L False)
    , ((mod4Mask,                 xK_l   ), windowGo U False)
    , ((mod4Mask,                 xK_k ), windowGo D False)

    -- Swap adjacent windows
    , ((mod4Mask .|. controlMask, xK_semicolon), windowSwap R False)
    , ((mod4Mask .|. controlMask, xK_j ), windowSwap L False)
    , ((mod4Mask .|. controlMask, xK_l ), windowSwap U False)
    , ((mod4Mask .|. controlMask, xK_k ), windowSwap D False)

    -- Swap the focused window and the master window
    , ((mod4Mask .|. shiftMask, xK_Return), windows W.swapMaster)

    -- Shrink the master area
    , ((mod4Mask .|. shiftMask, xK_j     ), sendMessage Shrink)

    -- Expand the master area
    , ((mod4Mask .|. shiftMask, xK_semicolon), sendMessage Expand)

    -- Push window back into tiling
    -- , ((mod4Mask .|. shiftMask, xK_f     ), withFocused $ windows . W.sink)
    , ((mod4Mask .|. shiftMask, xK_f     ), withFocused toggleFloat)

    -- Increment the number of windows in the master area
    , ((mod4Mask              , xK_equal ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((mod4Mask              , xK_minus), sendMessage (IncMasterN (-1))) ]
--}}}




-- xmonad keys {{{
myXmonadKeys conf = M.fromList

    --  Reset the layouts on the current workspace to default
    -- , ((mod4Mask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    -- Quit xmonad
    [ ((mod4Mask .|. shiftMask, xK_Escape   ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((mod4Mask              , xK_Escape     ), spawn "xmonad --recompile; xmonad --restart")

     -- Rotate through the available layout algorithms
    , ((mod4Mask,               xK_z ), sendMessage NextLayout)]
    -- ]}}}




-- form the key array
myKeys = myLauncherKeys <+> myWindowKeys <+> myWorkspaceKeys <+> myXmonadKeys

--form the hooks array
-- myManageHook = manageDocks <+> (isFullscreen --> doFullFloat) <+> manageHook defaultConfig
-- myManageHook = (isFullscreen --> doFullFloat)


-- pass the config into main
main = do
    nScreens <- countScreens
    xmonad $ docks $ ewmhFullscreen . ewmh $ pagerHints $ def {
          terminal                  = myTerminal
        , focusFollowsMouse         = myFocusFollowsMouse
        , borderWidth               = myBorderWidth
        , workspaces                = withScreens nScreens $ wsArray
        , keys                      = myKeys
        , startupHook		        = myStartupHook
        , layoutHook                = myLayoutHook
        -- , manageHook                = myManageHook
    }


